//
//  Line.swift
//  CarMap
//
//  Created by Karl Sigiscar on 07/10/2017.
//  Copyright © 2017 Karl Sigiscar. All rights reserved.
//

import Foundation

struct Line {
    var name:String
    var stations:[Station]
}
