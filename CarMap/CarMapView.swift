//
//  CarMapView.swift
//  CarMap
//
//  Created by Karl Sigiscar on 24/10/2017.
//  Copyright © 2017 Karl Sigiscar. All rights reserved.
//

import MapKit

class CarMapView: MKMapView {
    
    var viewModel:ViewModel?
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let view = super.hitTest(point, with: event)
        if !(view is MKPinAnnotationView) && (view != nil) {
            viewModel?.isAddCarEnabled = false
        }
        return view
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
