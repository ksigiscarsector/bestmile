//
//  MapViewDelegate.swift
//  CarMap
//
//  Created by Karl Sigiscar on 07/10/2017.
//  Copyright © 2017 Karl Sigiscar. All rights reserved.
//

import Foundation
import MapKit

class MapViewDelegate : NSObject, MKMapViewDelegate {
    
    var viewModel:ViewModel!
    
    init(withViewModel viewModel:ViewModel) {
        self.viewModel = viewModel
    }
    
    // MARK: MKMapViewDelegate
    
    func mapViewDidFinishRenderingMap(_ mapView: MKMapView, fullyRendered: Bool) {
        if fullyRendered {
            self.viewModel.isPermanentCarActiveEnabled = true
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is Station {
            let reusedAnnotationView:MKPinAnnotationView? = mapView.dequeueReusableAnnotationView(withIdentifier: "stationAnnotationView") as? MKPinAnnotationView
            guard let annotationView = reusedAnnotationView else {
                return nil
            }
            annotationView.pinTintColor = UIColor.blue
            return annotationView
        }

        return nil
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if view.annotation is Station {
            guard let pinAnnotationView = view as? MKPinAnnotationView else {
                return
            }
            
            pinAnnotationView.pinTintColor = UIColor.red
            
            if let selectedStation = view.annotation as? Station {
                viewModel.select(station: selectedStation)
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if view.annotation is Station {
            guard let pinAnnotationView = view as? MKPinAnnotationView else {
                return
            }
            
            pinAnnotationView.pinTintColor = UIColor.blue
        }
    }
}
