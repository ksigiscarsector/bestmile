//
//  ViewController.swift
//  CarMap
//
//  Created by Karl Sigiscar on 07/10/2017.
//  Copyright © 2017 Karl Sigiscar. All rights reserved.
//

import UIKit
import MapKit
import SpriteKit

class ViewController: UIViewController {

    @objc let viewModel = ViewModel()
    var mapViewContextString = "mapViewContext"
    var mapViewContext:UnsafeMutableRawPointer!
    var overlayScene:SKScene!
    var permanentCarNode:Vehicle? = nil

    @IBOutlet weak var mapView: CarMapView!
    @IBOutlet weak var spriteKitView: SKView!
    @IBOutlet weak var addCarButtonItem: UIBarButtonItem!
    @IBOutlet weak var activatePermanentCarButtonItem: UIBarButtonItem!
    @IBOutlet weak var currentNumberOfVehicleOnTheMapLabel: UILabel!
    @IBOutlet weak var totalNumberOfVehicleCreatedLabel: UILabel!
    @IBOutlet weak var totalTimeSpentByAllVehiclesLabel: UILabel!
    
    @IBAction func addCar(_ sender: Any) {
        
        if !viewModel.isMaximumNumberOfCarsReached() {
            viewModel.increaseNumberOfCarsOnTheMap(updateTotal: true)
            
            if let selectedStation = viewModel.selectedStation {
                if let station = viewModel.getAnotherStation(referenceStation: selectedStation) {
                    
                    let coordinate = station.coordinate
                    let startPoint = viewModel.convertMapPointToScenePoint(coordinate: coordinate, mapView:mapView, spriteKitView:spriteKitView, overlayScene:overlayScene)
                    
                    if let selectedStation = viewModel.selectedStation {
                        let endPoint = viewModel.convertMapPointToScenePoint(coordinate:selectedStation.coordinate, mapView:mapView, spriteKitView:spriteKitView, overlayScene:overlayScene)
                        let carNode = Vehicle()
                        viewModel.add(vehicle: carNode)
                        carNode.position = startPoint
                        overlayScene.addChild(carNode)
                        let moveAction = SKAction.move(to: endPoint, duration: 5.0)
                        carNode.run(moveAction, completion: { [weak self] in
                            let fadeOutAction = SKAction.fadeOut(withDuration:1)
                            carNode.run(fadeOutAction, completion: {
                                carNode.removeFromParent()
                                self?.viewModel.remove(vehicle: carNode)
                                self?.viewModel.decreaseNumberOfCarsOnTheMap()
                                self?.updateTextFields()
                            })
                        })
                        updateTextFields()
                    }
                }
            }
        }
    }
    
    func updateTextFields() {
        self.viewModel.updateTextFields(currentNumberOfVehicleOnTheMapLabel: currentNumberOfVehicleOnTheMapLabel, totalNumberOfVehicleCreatedLabel: totalNumberOfVehicleCreatedLabel, totalTimeSpentByAllVehiclesLabel:totalTimeSpentByAllVehiclesLabel)
    }
    
    @IBAction func activatePermanentCar(_ sender: Any) {
        if !viewModel.isPermanentCarActive {
            viewModel.activatePermamentCar()
            permanentCarNode = Vehicle()
            if let carNode = permanentCarNode {
                overlayScene.addChild(carNode)
                viewModel.increaseNumberOfCarsOnTheMap(updateTotal: false)
                if let startStation = viewModel.getAnotherStation(referenceStation: nil) {
                    movePermanent(car:carNode, startStation:startStation)
                }
            }
        } else {
            let fadeOutAction = SKAction.fadeOut(withDuration:1)
            permanentCarNode?.run(fadeOutAction, completion: { [weak self] in
                self?.permanentCarNode?.removeFromParent()
                self?.viewModel.deactivatePermamentCar()
                self?.viewModel.decreaseNumberOfCarsOnTheMap()
                self?.updateTextFields()
            })
        }
    }
    
    func movePermanent(car carNode:Vehicle, startStation:Station) {
        if let endStation = viewModel.getAnotherStation(referenceStation: startStation) {
            let startPoint = viewModel.convertMapPointToScenePoint(coordinate: startStation.coordinate, mapView:mapView, spriteKitView:spriteKitView, overlayScene:overlayScene)
            let endPoint = viewModel.convertMapPointToScenePoint(coordinate:endStation.coordinate, mapView:mapView, spriteKitView:spriteKitView, overlayScene:overlayScene)
            carNode.position = startPoint
            let moveAction = SKAction.move(to: endPoint, duration: 5.0)
            carNode.run(moveAction, completion: { [weak self] in
                let waitAction = SKAction.wait(forDuration: 2)
                carNode.run(waitAction, completion: {
                    self?.movePermanent(car:carNode, startStation:endStation)
                })
            })
            updateTextFields()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        let center = CLLocationCoordinate2D(latitude: CLLocationDegrees(46.5197), longitude: CLLocationDegrees(6.6323))
        let zoomLevel = CLLocationDegrees(0.03)
        let span = MKCoordinateSpan(latitudeDelta: zoomLevel, longitudeDelta: zoomLevel)
        viewModel.configure(mapView: mapView, center: center, span: span)
        mapView.viewModel = viewModel
        spriteKitView.allowsTransparency = true
        overlayScene = SKScene(size: mapView.frame.size)
        overlayScene.backgroundColor = UIColor.clear
        spriteKitView.presentScene(overlayScene)
        
        // Data binding
        
        mapViewContext = UnsafeMutableRawPointer(&mapViewContextString)
        addObserver(self, forKeyPath: #keyPath(viewModel.isAddCarEnabled), options: [.new], context: mapViewContext)
        addObserver(self, forKeyPath: #keyPath(viewModel.isPermanentCarActiveEnabled), options: [.new], context: mapViewContext)
        addObserver(self, forKeyPath: #keyPath(viewModel.totalTimeSpentByAllVehicles), options: [.new], context: mapViewContext)
        addObserver(self, forKeyPath: #keyPath(viewModel.permanentCarActiveLabelText), options: [.new], context: mapViewContext)

        // Add random annotations
        viewModel.createRandomStations(forMapView: mapView, center:center, zoomLevel:zoomLevel)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {

        if context == mapViewContext {
            if keyPath == #keyPath(viewModel.isAddCarEnabled) {
                addCarButtonItem.isEnabled = viewModel.isAddCarEnabled
            } else if keyPath == #keyPath(viewModel.isPermanentCarActiveEnabled) {
                activatePermanentCarButtonItem.isEnabled = viewModel.isPermanentCarActiveEnabled
            } else if keyPath == #keyPath(viewModel.totalTimeSpentByAllVehicles) {
                updateTextFields()
            } else if keyPath == #keyPath(viewModel.permanentCarActiveLabelText) {
                activatePermanentCarButtonItem.title = viewModel.permanentCarActiveLabelText
            }
        }
    }
}
