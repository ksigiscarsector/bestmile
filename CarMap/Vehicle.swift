//
//  Vehicle.swift
//  CarMap
//
//  Created by Karl Sigiscar on 23/10/2017.
//  Copyright © 2017 Karl Sigiscar. All rights reserved.
//

import SpriteKit

class Vehicle: SKSpriteNode {
    
    var startTime:TimeInterval
    
    init() {
        startTime = NSDate().timeIntervalSince1970
        let carTexture = SKTexture(imageNamed: "city-car")
        super.init(texture: carTexture, color: UIColor.clear, size: carTexture.size())
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
