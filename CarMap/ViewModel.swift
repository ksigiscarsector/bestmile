//
//  ViewModel.swift
//  CarMap
//
//  Created by Karl Sigiscar on 07/10/2017.
//  Copyright © 2017 Karl Sigiscar. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import SpriteKit

class ViewModel : NSObject {
    
    var model:Model!
    var timer:Timer!
    var mapDelegate:MapViewDelegate!

    @objc dynamic var isAddCarEnabled:Bool = false
    @objc dynamic var isPermanentCarActiveEnabled:Bool = false
    @objc dynamic var totalTimeSpentByAllVehicles:TimeInterval = 0
    @objc dynamic var permanentCarActiveLabelText:String = "Activate Permanent Car"
    
    override init() {
        super.init()
        model = Model()
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { [weak self] timer in
            if let time = self?.getTotalTimeSpentByAllVehicles() {
                self?.totalTimeSpentByAllVehicles = time
            }
        })
    }
    
    func add(line:Line) {
        model.add(line:line)
    }
    
    func getAnotherStation(referenceStation:Station?) -> Station? {
        var station:Station?
        var isSameStation = true
        
        while isSameStation {
            var line:Line = model.lines[0]
            let index = random(upperBound: 0, lowerBound: Double(line.stations.count))
            station = line.stations[Int(index)]
            isSameStation = (station == referenceStation)
            
            if !isSameStation {
                return station
            }
        }
        
        return nil
    }
    
    // MARK: Map configuration
    
    func configure(mapView:MKMapView, center:CLLocationCoordinate2D, span:MKCoordinateSpan) {
        mapDelegate = MapViewDelegate(withViewModel:self)
        mapView.region = MKCoordinateRegion(center: center, span: span)
        mapView.register(MKPinAnnotationView.self, forAnnotationViewWithReuseIdentifier: "stationAnnotationView")
        mapView.register(MKAnnotationView.self, forAnnotationViewWithReuseIdentifier: "carAnnotationView")
        mapView.delegate = mapDelegate
        mapView.isZoomEnabled = false
        mapView.isPitchEnabled = false
        mapView.isRotateEnabled = false
        mapView.isScrollEnabled = false
    }
    
    func createRandomStations(forMapView mapView:MKMapView, center:CLLocationCoordinate2D, zoomLevel:CLLocationDegrees) {
        var stations = [Station]()
        
        for _ in 1...5 {
            let latitudeRandomOffset = CLLocationDegrees(random(upperBound: zoomLevel / 3, lowerBound: -zoomLevel / 3))
            let longitudeRandomOffset = CLLocationDegrees(random(upperBound: zoomLevel / 3, lowerBound: -zoomLevel / 3))
            let location = CLLocationCoordinate2D(latitude: center.latitude + latitudeRandomOffset, longitude: center.longitude + longitudeRandomOffset)
            let station = Station(withLocation:location, connectingStations:[])
            mapView.addAnnotation(station)
            stations.append(station)
        }
        
        let line1 = Line(name: "Lausanne Line 1", stations: stations)
        add(line:line1)
    }
    
    func updatePermanentCarActiveEnabled() {
        isPermanentCarActiveEnabled = (model.currentNumberOfVehicleOnTheMap <= model.maximumNumberOfCars - 1)
    }

    // MARK: Utility functions
    
    func distance(between location1:CLLocationCoordinate2D, and location2:CLLocationCoordinate2D) -> CLLocationDegrees {
        let aSquare = pow(abs(location2.latitude - location1.latitude), 2)
        let bSquare = pow(abs(location2.longitude - location1.longitude), 2)
        return sqrt(aSquare + bSquare)
    }
    
    func random(upperBound:Double, lowerBound:Double) -> Float32 {
        let arc4randoMax:Double = 0x100000000
        let ab = Float32((Double(arc4random()) / arc4randoMax) * (upperBound - lowerBound) + lowerBound)
        return ab
    }
    
    func updateTextFields(currentNumberOfVehicleOnTheMapLabel:UILabel, totalNumberOfVehicleCreatedLabel:UILabel, totalTimeSpentByAllVehiclesLabel:UILabel) {
        currentNumberOfVehicleOnTheMapLabel.text = "Current: \(currentNumberOfVehicleOnTheMap)"
        totalNumberOfVehicleCreatedLabel.text = "Total: \(totalNumberOfVehicleCreated)"
        totalTimeSpentByAllVehiclesLabel.text = "Total time: \(stringFromTimeInterval(interval: totalTimeSpentByAllVehicles))s"
    }
    
    func stringFromTimeInterval(interval: TimeInterval) -> String {
        let interval = Int(interval)
        let seconds = interval % 60
        let minutes = (interval / 60) % 60
        return String(format: "%02d:%02d", minutes, seconds)
    }
    
    func convertMapPointToScenePoint(coordinate:CLLocationCoordinate2D, mapView:MKMapView, spriteKitView:SKView, overlayScene:SKScene) -> CGPoint {
        let mapPoint = mapView.convert(coordinate, toPointTo: spriteKitView)
        let scenePoint = spriteKitView.convert(mapPoint, to: overlayScene)
        return scenePoint
    }
    
    // MARK: Model Update
    
    func isMaximumNumberOfCarsReached() -> Bool {
        return model.currentNumberOfVehicleOnTheMap == model.maximumNumberOfCars
    }
    
    private func increaseTotalNumberOfVehicleCreated() {
        model.totalNumberOfVehicleCreated += 1
    }
    
    func increaseNumberOfCarsOnTheMap(updateTotal:Bool) {
        model.currentNumberOfVehicleOnTheMap += 1
        if updateTotal {
            increaseTotalNumberOfVehicleCreated()
        }
        updatePermanentCarActiveEnabled()
    }
    
    func decreaseNumberOfCarsOnTheMap() {
        model.currentNumberOfVehicleOnTheMap -= 1
        updatePermanentCarActiveEnabled()
    }
    
    func add(vehicle:Vehicle) {
        model.add(vehicle: vehicle)
    }
    
    func remove(vehicle:Vehicle) {
        model.remove(vehicle: vehicle)
    }
    
    func activatePermamentCar() {
        model.isPermanentCarActive = true
        permanentCarActiveLabelText = "De-activate Permanent Car"
    }
    
    func deactivatePermamentCar() {
        model.isPermanentCarActive = false
        permanentCarActiveLabelText = "Activate Permanent Car"
    }

    func getTotalTimeSpentByAllVehicles() -> TimeInterval {
        var result:TimeInterval = 0
        
        for vehicle in model.vehicles {
            let startTime = vehicle.startTime
            let currentTime = NSDate().timeIntervalSince1970 - startTime
            result += currentTime
        }
        
        return result
    }

    func select(station:Station) {
        model.selectedStation = station
        isAddCarEnabled = true
    }
    
    // MARK : Getters
    
    var isPermanentCarActive:Bool {
        get {
            return model.isPermanentCarActive
        }
    }
    
    var currentNumberOfVehicleOnTheMap:Int {
        get {
            return model.currentNumberOfVehicleOnTheMap
        }
    }
    
    var totalNumberOfVehicleCreated:Int {
        get {
            return model.totalNumberOfVehicleCreated
        }
    }
    
    var selectedStation:Station? {
        get {
            return model.selectedStation
        }
    }
}
