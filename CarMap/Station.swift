//
//  Station.swift
//  CarMap
//
//  Created by Karl Sigiscar on 07/10/2017.
//  Copyright © 2017 Karl Sigiscar. All rights reserved.
//

import Foundation
import MapKit

class Station : NSObject, MKAnnotation {

    let coordinate:CLLocationCoordinate2D
    let connectingStations:[Station]
    
    init(withLocation location:CLLocationCoordinate2D, connectingStations:[Station]) {
        self.coordinate = location
        self.connectingStations = connectingStations
    }
}
