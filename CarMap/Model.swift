//
//  Model.swift
//  CarMap
//
//  Created by Karl Sigiscar on 07/10/2017.
//  Copyright © 2017 Karl Sigiscar. All rights reserved.
//

import Foundation

class Model : NSObject {
    let maximumNumberOfCars = 10
    var currentNumberOfVehicleOnTheMap:Int = 0
    var totalNumberOfVehicleCreated:Int = 0
    var isPermanentCarActive:Bool = false

    var selectedStation:Station? = nil

    var lines = [Line]()
    var vehicles = [Vehicle]()
    
    func add(line:Line) {
        lines.append(line)
    }
    
    func add(vehicle:Vehicle) {
        vehicles.append(vehicle)
    }
    
    func remove(vehicle:Vehicle) {
        if let index = vehicles.index(of: vehicle) {
            vehicles.remove(at: index)
        }
    }
}
