//
//  CarMapTests.swift
//  CarMapTests
//
//  Created by Karl Sigiscar on 07/10/2017.
//  Copyright © 2017 Karl Sigiscar. All rights reserved.
//

import XCTest
import MapKit

class CarMapTests: XCTestCase {
    
    let viewModel = ViewModel()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAddTenCars() {
        for _  in 1...10 {
            viewModel.increaseNumberOfCarsOnTheMap(updateTotal: true)
        }
        XCTAssertEqual(viewModel.currentNumberOfVehicleOnTheMap, 10)
    }
    
    func testAddTenCarsThenRemoveFive() {
        for _  in 1...10 {
            viewModel.increaseNumberOfCarsOnTheMap(updateTotal: true)
        }
        
        for _ in 1...5 {
            viewModel.decreaseNumberOfCarsOnTheMap()
        }
        
        XCTAssertEqual(viewModel.currentNumberOfVehicleOnTheMap, 5)
    }
    
    func testTotalNumberOfCarsCreated() {
        for _  in 1...10 {
            viewModel.increaseNumberOfCarsOnTheMap(updateTotal: true)
        }

        for _ in 1...5 {
            viewModel.decreaseNumberOfCarsOnTheMap()
        }
        
        for _  in 1...10 {
            viewModel.increaseNumberOfCarsOnTheMap(updateTotal: true)
        }
        
        XCTAssertEqual(viewModel.totalNumberOfVehicleCreated, 20)
    }
    
    func testPermanentCarActiveDisabled() {
        for _  in 1...10 {
            viewModel.increaseNumberOfCarsOnTheMap(updateTotal: true)
        }
        XCTAssertFalse(viewModel.isPermanentCarActiveEnabled)
    }
    
    func testPermanentCarActiveEnabled() {
        for _  in 1...9 {
            viewModel.increaseNumberOfCarsOnTheMap(updateTotal: true)
        }
        XCTAssertTrue(viewModel.isPermanentCarActiveEnabled)
    }

    func testAddCarDisabled() {
        XCTAssertFalse(viewModel.isAddCarEnabled)
    }
    
    func testAddCarEnabled() {
        let location = CLLocationCoordinate2D(latitude:0, longitude:0)
        let station = Station(withLocation:location, connectingStations: [])
        viewModel.select(station: station)
        XCTAssertTrue(viewModel.isAddCarEnabled)
    }
}
